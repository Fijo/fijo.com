using Fijo.Com.UI.Adapter;
using Fijo.Com.UI.Enums;
using NUnit.Framework;

namespace Fijo.Com.UI.Test {
	[TestFixture]
	public class InputServiceTest {
		[Test, Ignore("manual (integration) test")]
		public void ClickTest() {
			var inputService = new InputAdapter();
			inputService.SendClick(MouseFlags.Right | MouseFlags.Down);
			inputService.SendClick(MouseFlags.Right | MouseFlags.Up);
		}

		[Test, Ignore("manual (integration) test")]
		public void KeyTest() {
			var inputService = new InputAdapter();
			inputService.SendKey(KeyCode.KeyJ, KeyEventFlags.Down);
			inputService.SendKey(KeyCode.KeyJ, KeyEventFlags.Up);
		}

		[Test, Ignore("manual (integration) test")]
		public void ScrollTest() {
			var inputService = new InputAdapter();
			inputService.SendScroll(ScrollDirection.Vertical, -1);
		}
	}
}