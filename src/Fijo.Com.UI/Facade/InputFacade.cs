using System.Drawing;
using System.Globalization;
using Fijo.Com.UI.Enums;
using Fijo.Com.UI.Facade.Interface;
using Fijo.Com.UI.Services;
using Fijo.Com.UI.Services.Interface;

namespace Fijo.Com.UI.Facade {
	public class InputFacade : IInputFacade {
		private readonly IInputService _inputService;
		private readonly IStringInputService _stringInputService;

		public InputFacade(IInputService inputService, IStringInputService stringInputService) {
			_inputService = inputService;
			_stringInputService = stringInputService;
		}

		#region Implementation of IInputFacade
		public void SetPosition(Point target) {
			_inputService.SetPosition(target);
		}

		public Point GetPosition() {
			return _inputService.GetPosition();
		}

		public void SendClick(MouseFlags mouseFlags) {
			_inputService.SendClick(mouseFlags);
		}

		public void SendScroll(ScrollDirection scrollDirection, int movement, bool sendOneEvent = false) {
			_inputService.SendScroll(scrollDirection, movement, sendOneEvent);
		}

		public void SendKey(KeyCode keyCode, KeyEventFlags keyEventFlags) {
			_inputService.SendKey(keyCode, keyEventFlags);
		}

		public void SendKey(KeyCode keyCode) {
			_inputService.SendKey(keyCode);
		}

		public void SendKey(string value, CultureInfo cultureInfo) {
			_stringInputService.SendKeys(value, cultureInfo);
		}

		public void SendKeysInvariant(string value) {
			_stringInputService.SendKeysInvariant(value);
		}
		#endregion
	}
}