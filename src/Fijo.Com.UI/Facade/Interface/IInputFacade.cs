using System.Drawing;
using System.Globalization;
using Fijo.Com.UI.Enums;
using JetBrains.Annotations;

namespace Fijo.Com.UI.Facade.Interface {
	public interface IInputFacade {
		void SetPosition(Point target);
		Point GetPosition();
		void SendClick(MouseFlags mouseFlags);
		void SendScroll(ScrollDirection scrollDirection, int movement, bool sendOneEvent = false);
		void SendKey(KeyCode keyCode, KeyEventFlags keyEventFlags);
		void SendKey(KeyCode keyCode);
		void SendKey(string value, [NotNull] CultureInfo cultureInfo);
		void SendKeysInvariant(string keys);
	}
}