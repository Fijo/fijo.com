using System;
using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace Fijo.Com.UI.Dto {
	[Dto]
	public class CharInputRule {
		public readonly ICollection<SendKey> SendKeys;
		public readonly char Char;

		public CharInputRule([NotNull] ICollection<SendKey> sendKeys, Char c) {
			SendKeys = sendKeys;
			Char = c;
		}
	}
}