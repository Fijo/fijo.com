using Fijo.Com.UI.Enums;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;

namespace Fijo.Com.UI.Dto {
	[Dto, Note(NoteType.Default, "may be a struct for performance reasons but left as a class for readablility and place of optimization resons.")]
	public class SendKey {
		public readonly KeyCode KeyCode;
		public readonly KeyEventFlags KeyEventFlags;

		public SendKey(KeyCode keyCode, KeyEventFlags keyEventFlags) {
			KeyCode = keyCode;
			KeyEventFlags = keyEventFlags;
		}
	}
}