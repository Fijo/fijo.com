using System.Threading;
using Fijo.Com.UI.Dto;
using Fijo.Com.UI.Helper.Interface;
using Fijo.Infrastructure.DesignPattern.Repository;

namespace Fijo.Com.UI.Helper {
	public class InputHelper : IInputHelper {
		private readonly InputConfiguration _inputConfiguration;

		public InputHelper(IRepository<InputConfiguration> inputConfigurationRepository) {
			_inputConfiguration = inputConfigurationRepository.Get();
		}

		#region Implementation of IInputHelper
		public void WaitBetweenSendInput() {
			Thread.Sleep(_inputConfiguration.DelayBetweenSendInput);
		}
		#endregion
	}
}