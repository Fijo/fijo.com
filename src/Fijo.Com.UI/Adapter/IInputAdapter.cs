using System.Drawing;
using Fijo.Com.UI.Enums;

namespace Fijo.Com.UI.Adapter {
	public interface IInputAdapter {
		void SetPosition(Point target);
		Point GetPosition();
		void SendClick(MouseFlags mouseFlags);
		void SendScroll(ScrollDirection scrollDirection, int movement);
		void SendKey(KeyCode keyCode, KeyEventFlags keyEventFlags);
	}
}