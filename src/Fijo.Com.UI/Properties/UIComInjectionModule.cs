using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using JetBrains.Annotations;

namespace Fijo.Com.UI.Properties {
	[PublicAPI]
	public class UIComInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new InputUIComInjectionModule());
			kernel.Load(new WindowUIComInjectionModule());
		}
	}
}