using Fijo.Com.UI.Services;
using Fijo.Com.UI.Services.Interface;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using JetBrains.Annotations;

namespace Fijo.Com.UI.Properties {
	[PublicAPI]
	public class WindowUIComInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new UIComBasicInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<IWindowService>().To<WindowService>().InSingletonScope();
		}
	}
}