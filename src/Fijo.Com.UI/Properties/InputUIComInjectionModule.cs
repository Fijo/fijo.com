using Fijo.Com.UI.Adapter;
using Fijo.Com.UI.Dto;
using Fijo.Com.UI.Facade;
using Fijo.Com.UI.Facade.Interface;
using Fijo.Com.UI.Helper;
using Fijo.Com.UI.Helper.Interface;
using Fijo.Com.UI.Provider;
using Fijo.Com.UI.Provider.Interface;
using Fijo.Com.UI.Repository;
using Fijo.Com.UI.Services;
using Fijo.Com.UI.Services.Interface;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using JetBrains.Annotations;

namespace Fijo.Com.UI.Properties {
	[PublicAPI]
	public class InputUIComInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<IInputAdapter>().To<InputAdapter>().InSingletonScope();

			kernel.Bind<IRepository<InputConfiguration>>().To<InputConfigurationRepository>().InSingletonScope();

			kernel.Bind<IInputHelper>().To<InputHelper>().InSingletonScope();
			kernel.Bind<IInputService>().To<InputService>().InSingletonScope();

			kernel.Bind<ICharInputRuleProvider>().To<CharInputRuleProvider>().InSingletonScope();

			kernel.Bind<ICharInputProvider>().To<CharInputProvider>().InSingletonScope();

			kernel.Bind<ICharInputService>().To<CharInputService>().InSingletonScope();
			kernel.Bind<IStringInputService>().To<StringInputService>().InSingletonScope();

			kernel.Bind<IInputFacade>().To<InputFacade>().InSingletonScope();
		}
	}
}