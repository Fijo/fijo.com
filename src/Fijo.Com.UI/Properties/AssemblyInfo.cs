﻿using System.Reflection;
using System.Runtime.InteropServices;
using FijoCore.Infrastructure.LightContrib.Default.Attributes;

// Allgemeine Informationen über eine Assembly werden über die folgenden 
// Attribute gesteuert. Ändern Sie diese Attributwerte, um die Informationen zu ändern,
// die mit einer Assembly verknüpft sind.
[assembly: AssemblyTitle("Fijo.Com.UI")]
[assembly: AssemblyDescription("This library provides functionality to access to the UI of an/ another application.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fijo")]
[assembly: AssemblyProduct("Fijo.Com.UI")]
[assembly: AssemblyCopyright("Copyright © Jonas Fischer <fijo.com@googlemail.com> 2013")]
[assembly: AssemblyTrademark("Fijo")]
[assembly: AssemblyCulture("")]

#region AssemblyEnableDefenition
#if DEBUG
[assembly: AssemblyEnableDefenitionAttibute("DEBUG")]
#endif

#if TRACE
[assembly: AssemblyEnableDefenitionAttibute("TRACE")]
#endif
#endregion

// Durch Festlegen von ComVisible auf "false" werden die Typen in dieser Assembly unsichtbar 
// für COM-Komponenten. Wenn Sie auf einen Typ in dieser Assembly von 
// COM zugreifen müssen, legen Sie das ComVisible-Attribut für diesen Typ auf "true" fest.
[assembly: ComVisible(false)]

// Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird
[assembly: Guid("c9388796-6289-4e37-9dbe-038870d4e158")]

// Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
//
//      Hauptversion
//      Nebenversion 
//      Buildnummer
//      Revision
//
// Sie können alle Werte angeben oder die standardmäßigen Build- und Revisionsnummern 
// übernehmen, indem Sie "*" eingeben:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
