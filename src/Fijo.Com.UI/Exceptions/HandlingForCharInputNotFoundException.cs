using System;
using System.Globalization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using JetBrains.Annotations;

namespace Fijo.Com.UI.Exceptions {
	[Serializable, PublicAPI, About("CharInputRuleNotFoundException")]
	public class HandlingForCharInputNotFoundException : KeyNotFoundException {
		[PublicAPI]
		public CultureInfo CultureInfoKey { get; set; }

		public HandlingForCharInputNotFoundException(object source, [NotNull] CultureInfo cultureInfoKey, char key, string message = default(string), Exception innerException = null) : base(source, key, message, innerException) {
			CultureInfoKey = cultureInfoKey;
		}
	}
}