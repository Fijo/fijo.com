using System.Globalization;
using Fijo.Com.UI.Helper;
using Fijo.Com.UI.Helper.Interface;
using Fijo.Com.UI.Services.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.Com.UI.Services {
	public class StringInputService : IStringInputService {
		private readonly ICharInputService _charInputService;
		private readonly IInputHelper _inputHelper;

		public StringInputService(ICharInputService charInputService, IInputHelper inputHelper) {
			_charInputService = charInputService;
			_inputHelper = inputHelper;
		}

		#region Implementation of IStringInputService
		public void SendKeys(string keys, CultureInfo cultureInfo) {
			InternalSendKeys(keys, cultureInfo);
		}

		public void SendKeysInvariant(string keys) {
			InternalSendKeys(keys, CultureInfo.InvariantCulture);
		}

		private void InternalSendKeys(string keys, CultureInfo cultureInfo) {
			keys.ForEachAndBetween(c => _charInputService.SendKeys(c, cultureInfo),
			                       _inputHelper.WaitBetweenSendInput);
		}
		#endregion
	}
}