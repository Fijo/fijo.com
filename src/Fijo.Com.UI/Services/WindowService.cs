using System;
using System.Drawing;
using System.Runtime.InteropServices;
using Fijo.Com.UI.Services.Interface;

namespace Fijo.Com.UI.Services {
	public class WindowService : IWindowService {
		#region Implementation of IWindowService
		public bool FocusWindow(IntPtr windowHandle) {
			return SetForegroundWindow(windowHandle);
		}

		public IntPtr SetActive(IntPtr windowHandle) {
			return SetActiveWindow(windowHandle);
		}

		public bool TryGetWindowRect(HandleRef windowHandleRef, out Rectangle rectangle) {
			return GetWindowRect(windowHandleRef, out rectangle);
		}
		#endregion

		#region Extern
		[DllImport("User32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool SetForegroundWindow(IntPtr hwnd);
		
		[DllImport("user32.dll", SetLastError = true)]
		public static extern IntPtr SetActiveWindow(IntPtr hWnd);

		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool GetWindowRect(HandleRef hWnd, out Rectangle lpRect);
		#endregion
	}
}