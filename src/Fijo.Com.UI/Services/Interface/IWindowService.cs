using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Fijo.Com.UI.Services.Interface {
	public interface IWindowService {
		bool FocusWindow(IntPtr windowHandle);
		IntPtr SetActive(IntPtr windowHandle);
		bool TryGetWindowRect(HandleRef windowHandleRef, out Rectangle rectangle);
	}
}