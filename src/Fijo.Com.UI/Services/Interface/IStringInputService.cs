using System.Globalization;
using JetBrains.Annotations;

namespace Fijo.Com.UI.Services.Interface {
	public interface IStringInputService {
		void SendKeys(string keys, [NotNull] CultureInfo cultureInfo);
		void SendKeysInvariant(string keys);
	}
}