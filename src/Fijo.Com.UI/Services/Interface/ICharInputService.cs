using System.Globalization;
using JetBrains.Annotations;

namespace Fijo.Com.UI.Services.Interface {
	public interface ICharInputService {
		void SendKeys(char key, [NotNull] CultureInfo cultureInfo);
	}
}