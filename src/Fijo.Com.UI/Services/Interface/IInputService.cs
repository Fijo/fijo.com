using System.Drawing;
using Fijo.Com.UI.Enums;

namespace Fijo.Com.UI.Services.Interface {
	public interface IInputService {
		void SetPosition(Point target);
		Point GetPosition();
		void SendClick(MouseFlags mouseFlags);
		void SendScroll(ScrollDirection scrollDirection, int movement, bool sendOneEvent = false);
		void SendKey(KeyCode keyCode, KeyEventFlags keyEventFlags);
		void SendKey(KeyCode keyCode);
	}
}