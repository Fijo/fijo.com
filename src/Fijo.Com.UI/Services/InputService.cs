using System;
using System.Drawing;
using System.Linq;
using Fijo.Com.UI.Adapter;
using Fijo.Com.UI.Enums;
using Fijo.Com.UI.Helper;
using Fijo.Com.UI.Helper.Interface;
using Fijo.Com.UI.Services.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;

namespace Fijo.Com.UI.Services {
	public class InputService : IInputService {
		private readonly IInputAdapter _inputAdapter;
		private readonly IInputHelper _inputHelper;

		public InputService(IInputAdapter inputAdapter, IInputHelper inputHelper) {
			_inputAdapter = inputAdapter;
			_inputHelper = inputHelper;
		}

		#region Implementation of IInputFacade
		public void SetPosition(Point target) {
			_inputAdapter.SetPosition(target);
		}

		public Point GetPosition() {
			return _inputAdapter.GetPosition();
		}

		#region SendClick
		public void SendClick(MouseFlags mouseFlags) {
			if(!IsBoth(mouseFlags)) InternalSendClick(mouseFlags);
			var mouseWithoutBoth = mouseFlags & ~MouseFlags.Both;
			InternalSendClick(mouseWithoutBoth | MouseFlags.Down);
			WaitBetweenSendInput();
			InternalSendClick(mouseWithoutBoth | MouseFlags.Up);
		}

		private void InternalSendClick(MouseFlags mouseFlags) {
			_inputAdapter.SendClick(mouseFlags);
		}

		private bool IsBoth(MouseFlags mouseFlags) {
			return mouseFlags.HasFlag(MouseFlags.Both) || (!mouseFlags.HasFlag(MouseFlags.Down) && !mouseFlags.HasFlag(MouseFlags.Up));
		}
		#endregion

		#region SendScroll
		public void SendScroll(ScrollDirection scrollDirection, int movement, bool sendOneEvent = false) {
			if(movement == 0) return;
			if(!sendOneEvent) {
				var absMovement = Math.Abs(movement);
				if(absMovement != 1) {
					SendSeveralScrollEvents(scrollDirection, movement, absMovement);
					return;
				}
			}
			InternalSendScroll(scrollDirection, movement);
		}

		private void SendSeveralScrollEvents(ScrollDirection scrollDirection, int movement, int absMovement) {
			var singleMovement = movement.IsNegative() ? -1 : 1;
			Enumerable.Range(0, absMovement)
				.ForEachAndBetween(x => InternalSendScroll(scrollDirection, singleMovement),
				                   WaitBetweenSendInput);
		}

		private void InternalSendScroll(ScrollDirection scrollDirection, int movement) {
			_inputAdapter.SendScroll(scrollDirection, movement);
		}
		#endregion

		#region SendKey
		public void SendKey(KeyCode keyCode, KeyEventFlags keyEventFlags) {
			_inputAdapter.SendKey(keyCode, keyEventFlags);
		}

		public void SendKey(KeyCode keyCode) {
			SendKey(keyCode, KeyEventFlags.Down);
			WaitBetweenSendInput();
			SendKey(keyCode, KeyEventFlags.Up);
		}
		#endregion
		#endregion

		private void WaitBetweenSendInput() {
			_inputHelper.WaitBetweenSendInput();
		}
	}
}