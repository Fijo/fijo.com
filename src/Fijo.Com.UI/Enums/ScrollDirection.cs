namespace Fijo.Com.UI.Enums {
	public enum ScrollDirection : byte {
		Horizontal = 0,
		Vertical = 1
	}
}