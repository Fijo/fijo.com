using System.Collections.Generic;
using System.Globalization;
using Fijo.Com.UI.Dto;
using JetBrains.Annotations;

namespace Fijo.Com.UI.Provider.Interface {
	public interface ICharInputRuleProvider {
		[NotNull]
		IEnumerable<CharInputRule> Get([NotNull] CultureInfo cultureInfo);
	}
}