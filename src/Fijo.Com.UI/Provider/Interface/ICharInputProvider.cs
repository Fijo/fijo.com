using System.Globalization;
using Fijo.Com.UI.Dto;
using JetBrains.Annotations;

namespace Fijo.Com.UI.Provider.Interface {
	public interface ICharInputProvider {
		[NotNull] CharInputRule Get(char c, [NotNull] CultureInfo cultureInfo);
	}
}