using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Fijo.Com.UI.Dto;
using Fijo.Com.UI.Exceptions;
using Fijo.Com.UI.Provider.Interface;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using JetBrains.Annotations;

namespace Fijo.Com.UI.Provider {
	public class CharInputProvider : ICharInputProvider {
		private readonly ICharInputRuleProvider _charInputRuleProvider;
		private readonly IDictionary<CultureInfo, IDictionary<char, CharInputRule>> _charInputRules = new Dictionary<CultureInfo, IDictionary<char, CharInputRule>>();
		private readonly string _handlingNotFoundMessage = string.Format("No CharInputRule could be found for your CharInput. Try to use other keys to write or check if the app is supported for your culture. The {0} did not give such a rule - you may have to extend it or used by it.", CN.Get<IRepository<IEnumerable<CharInputRule>>>());

		public CharInputProvider(ICharInputRuleProvider charInputRuleProvider) {
			_charInputRuleProvider = charInputRuleProvider;
		}

		public CharInputRule Get(char c, CultureInfo cultureInfo) {
			return _charInputRules.GetOrCreate(cultureInfo, () => (IDictionary<char, CharInputRule>) _charInputRuleProvider.Get(cultureInfo).ToDictionary(x => x.Char))
				.GetOrThrow(c, x => GetHandlingNotFoundException(_charInputRules, cultureInfo, x));
		}

		[NotNull]
		private Exception GetHandlingNotFoundException([NotNull] IDictionary<CultureInfo, IDictionary<char, CharInputRule>> charInputRules, [NotNull] CultureInfo cultureInfo, char c) {
			return new HandlingForCharInputNotFoundException(charInputRules, cultureInfo, c, _handlingNotFoundMessage);
		}
	}
}